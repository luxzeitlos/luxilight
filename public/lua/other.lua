ws2812.init()
buffer = ws2812.newBuffer(42, 3)
buffer:fill(0, 0, 0)
ws2812.write(buffer)

state = true

tmr.alarm(0, 1000, tmr.ALARM_AUTO, function ()
    if state then
        buffer:fill(128, 0, 255)
    else
        buffer:fill(255, 0, 128)
    end

    ws2812.write(buffer)

    state = not state
end)