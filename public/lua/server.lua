ws2812.init()

srv=net.createServer(net.TCP, 30)
srv:listen(80,function(conn)
    local income_buffer = ""
    function process()
        conn:on("sent",function(conn)
            conn:close()
        end)
    end

    conn:on("receive", function(conn,request)
        income_buffer = income_buffer .. request
        print("income_buffer:")
        print(income_buffer)
        local _, body_start = string.find(income_buffer, "\r\n\r\n")

        if (body_start ~= nil) then
            print("got body")
            -- Okey, we've read the entire header.

            local _, _, method, path, vars = string.find(income_buffer, "([A-Z]+) (.+)?(.+) HTTP");
            if(method == nil)then
                _, _, method, path = string.find(income_buffer, "([A-Z]+) (.+) HTTP");
            end

            print("method: ".. method .. "\npath: " .. path)

            local _, _, content_length = string.find(income_buffer, "Content%-Length: ([0-9]+)");
            print("content_length:")
            print(content_length)
            print(tonumber(content_length))

            print("total size")
            print(body_start + tonumber(content_length))

            print("real size")
            print(string.len(income_buffer))

            if(string.len(income_buffer) == body_start + tonumber(content_length)) then
                -- okay, we've read the entire body.
                local body = string.sub(income_buffer, body_start + 1)
                print("body:")
                print(body)

                print("body len")
                print(string.len(body))
                ws2812.write(body)
            end

            conn:send("HTTP/1.0 200 OK\r\n\r\n")
            conn:on("sent",function(conn)
                conn:close()
                collectgarbage()
            end)
        end
    end)
end)
